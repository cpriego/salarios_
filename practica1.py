# Salarios 
import pexpect


nomina1 = 30000
nombre1 = "Ana" 

def calculo_impuesto(nomina1, nombre1):
    impuesto1 = nomina1 * 0.3
    return impuesto1git 

def calculo_coste_salarial(nomina1, impuesto1):
    coste_salarial1 = nomina1 + impuesto1
    return coste_salarial1

nomina2 = 20000
nombre2 = "Pepe"

def calculo_impuesto(nomina2, nombre2):
    impuesto2 = nomina2 * 0.3
    return impuesto2

def calculo_coste_salarial(nomina2, impuesto2):
    coste_salarial2 = nomina2 + impuesto2
    return coste_salarial2

impuesto1 = calculo_impuesto(nomina1, nombre1)
coste_salarial1 = calculo_coste_salarial(nomina1, impuesto1)
print("El impuesto de Ana es ", impuesto1, "y el coste salarial es ", coste_salarial1)

impuesto2 = calculo_impuesto(nomina2, nombre2)
coste_salarial2 = calculo_coste_salarial(nomina2, impuesto2)
print("El impuesto de Pepe es ", impuesto2, "y el coste salarial es ", coste_salarial2)